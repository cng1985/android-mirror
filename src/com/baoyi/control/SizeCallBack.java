package com.baoyi.control;

abstract class SizeCallBack {
	abstract void onSizeChanged(int w, int h);
}
