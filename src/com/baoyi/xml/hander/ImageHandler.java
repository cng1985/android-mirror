package com.baoyi.xml.hander;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.baoyi.domain.Pic;

public class ImageHandler extends DefaultHandler {

	private List<Pic> pics;
	private String img;
	private String typefolder;
	private String folder;
	public List<Pic> getPics() {
		return pics;
	}

	public void setPics(List<Pic> pics) {
		this.pics = pics;
	}

	public ImageHandler(String type) {
		pics = new ArrayList<Pic>();
		typefolder=type;
	}

	@Override
	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		// TODO Auto-generated method stub
		System.out.println("name:" + name);
		if(localName.equals("folder")){
			folder = attributes.getValue("type");
		}
		if(null!=folder&&folder.equals(typefolder)){
			if (localName.equals("img")) {
				img = attributes.getValue("file");
			}
		}
		
	}

	@Override
	public void endElement(String uri, String localName, String name)
			throws SAXException {
		if(null!=folder&&folder.equals(typefolder)){
			if (localName.equals("img")) {
				Pic p = new Pic();
				p.setId(typefolder+"/"+img);
				p.setName(img);
				p.setCatalogs(typefolder);
				pics.add(p);
			}
		}
	}
}
