package com.baoyi.domain;

import java.sql.Date;

import com.baoyi.content.content;

public class Pic {

	private String id;
	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private Date starDate;
	private Date updateDate;
	private Long score;
	private String catalogs;

	public String getCatalogs() {
		return catalogs;
	}
	public String getUrl(){
		String url = content.picserver+ "imgs/" + id;
	   return url;
   }
	public void setCatalogs(String catalogs) {
		this.catalogs = catalogs;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getStarDate() {
		if(null==starDate){
			starDate=new Date(System.currentTimeMillis());
		}
		return starDate;
	}

	public void setStarDate(Date starDate) {
		this.starDate = starDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public Long getScore() {
		return score;
	}

	public void setScore(Long score) {
		this.score = score;
	}

}
