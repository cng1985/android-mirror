package com.baoyi.adapter;

import android.widget.BaseAdapter;

public abstract class BaoyiBaseAdapter extends BaseAdapter {

	public abstract String getPicId(int position);
	public abstract String getCataLogs(int position);
}
