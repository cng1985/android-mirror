package com.baoyi.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MessageList {

	private static List<String> messages;
	static {
		messages = new ArrayList<String>();
		messages.add("哇塞，你的眼光不错哦！");
		messages.add("看来各位童鞋都很稀罕这张哦！");
		messages.add("给力美女，你来做主！");
		messages.add("哎呦，不错哦！");
		messages.add("你真有眼光！");
		messages.add("美女如云，万里挑一，兄弟你很有眼光哦！");
		messages.add("这位童鞋，你的眼光真的很给力哦！");
		messages.add("小盆友，年纪轻轻竟有如此眼光，实乃可造之材！");
		messages.add("支持率如此之高，绝代佳人即将诞生了！");
		messages.add("撒花撒花喽，热烈祝贺这位童鞋和大众的眼光达成高度的一致！");
		messages.add("经鉴定，你的审美水平已经达到登峰造极的境界了！");
	}

	public static String getMessage() {
		Random rand = new Random();
		int i = rand.nextInt(messages.size());
		String message = messages.get(i);
		return message;
	}
}
