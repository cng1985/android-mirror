package com.baoyi.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.baoyi.content.content;
import com.baoyi.domain.Pic;
import com.baoyi.utils.AsyncImageLoader;
import com.baoyi.utils.Connection;
import com.baoyi.utils.HttpConnection;
import com.baoyi.utils.AsyncImageLoader.ImageCallback;
import com.baoyi.xml.hander.ImageHandler;

public class NetCachesImagesAdapter extends BaseAdapter {
	private Context mcontext;
	private List<Pic> files = new ArrayList<Pic>();
	private Map<String, Bitmap> caches = new HashMap<String, Bitmap>();
	private Map<String, ImageView> imgs = new HashMap<String, ImageView>();
	private String filename;
	private AsyncImageLoader imageLoader = new AsyncImageLoader();
	public NetCachesImagesAdapter(String file, Context mcontext) {
	

	}

	@Override
	public int getCount() {
		return files.size();
	}

	public ImageView getImageViewById(int id) {
		String file = files.get(id).getId();
		return imgs.get(file);
	}

	@Override
	public Object getItem(int position) {
		Bitmap image = null;
		// String file = filename + "/" + files.get(position);
		// image = getImageFromAssetFile(file, "");
		return image;
	}

	public String getPicId(int position) {
		String file = filename + "/" + files.get(position);
		return file;
	}

	public String getCataLogs(int position) {
		return filename;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView view = null;
		
		if (convertView == null) {
			view = new ImageView(mcontext);

		} else {
			view = (ImageView) convertView;
		}
		final ImageView viw=view;
		Pic pic = files.get(position);
		try {
			String file = content.server + "imgs/" + pic.getId();
			imageLoader.loadDrawable(file, new ImageCallback() {
				public void imageLoaded(Drawable imageDrawable, String imageUrl) {
					viw.setImageDrawable(imageDrawable);
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}

		return view;
	}


}
