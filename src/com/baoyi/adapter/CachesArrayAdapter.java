package com.baoyi.adapter;

import java.io.StringReader;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.baoyi.content.content;
import com.baoyi.domain.Pic;
import com.baoyi.utils.AsyncImageLoader;
import com.baoyi.utils.AsyncImageLoader.ImageCallback;
import com.baoyi.utils.Connection;
import com.baoyi.utils.HttpConnection;
import com.baoyi.xml.hander.ImageHandler;

public class CachesArrayAdapter extends ArrayAdapter<Pic> {


	public CachesArrayAdapter(Activity activity,Pic[] newsList) {
		super(activity, 0, newsList);
	}
   private void init(String file){
	   SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			String urls = content.server + "data/main_directory.xml";
			URL url = new URL(urls);
			Connection urlConn = HttpConnection.connect(url);
			SAXParser parser = factory.newSAXParser();
			XMLReader xmlreader = parser.getXMLReader();
			ImageHandler healHandler = new ImageHandler(file);
			xmlreader.setContentHandler(healHandler);
			String xmldata = urlConn.execute().body();
			xmlreader.parse(new InputSource(new StringReader(xmldata)));
			List<Pic> 	files = healHandler.getPics();
		} catch (Exception e) {
			e.printStackTrace();
		}
   }
	private AsyncImageLoader imageLoader = new AsyncImageLoader();

	private Map<Integer, View> viewMap = new HashMap<Integer, View>();

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = this.viewMap.get(position);
		final ImageView view = (ImageView) convertView;;
		if (rowView == null) {
			LayoutInflater inflater = ((Activity) this.getContext())
					.getLayoutInflater();
			// rowView = inflater.inflate(R.layout.news_row, null);
			Pic newsBean = this.getItem(position);
			String url="";
			imageLoader.loadDrawable(url, new ImageCallback() {

				public void imageLoaded(Drawable imageDrawable, String imageUrl) {
					view.setImageDrawable(imageDrawable);
				}
			});
			viewMap.put(position, view);
		}
		return rowView;
	}
}
