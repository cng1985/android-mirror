package com.baoyi.adapter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.baoyi.content.content;
import com.baoyi.domain.Pic;
import com.baoyi.factory.PicFacotry;
import com.baoyi.ui.MainUI;

public class NetImagesAdapter extends BaseAdapter {
	private Context mcontext;
	private List<Pic> files = new ArrayList<Pic>();
	private Map<String, Bitmap> caches = new HashMap<String, Bitmap>();
	private Map<String, ImageView> imgs = new HashMap<String, ImageView>();
	private String filename;

	public NetImagesAdapter(String file, Context mcontext) {
		files=PicFacotry.createByName(file);
	}

	@Override
	public int getCount() {
		return files.size();
	}

	public ImageView getImageViewById(int id) {
		String file = files.get(id).getId();
		return imgs.get(file);
	}

	@Override
	public Object getItem(int position) {
		Bitmap image = null;
		// String file = filename + "/" + files.get(position);
		// image = getImageFromAssetFile(file, "");
		return image;
	}

	public String getPicId(int position) {
		return files.get(position).getId();
	}

	public String getCataLogs(int position) {
		return files.get(position).getCatalogs();
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView view = null;
		if (convertView == null) {
			view = new ImageView(mcontext);

		} else {
			view = (ImageView) convertView;
		}
		Pic pic = files.get(position);
		try {
			view.setImageBitmap(getImageFromAssetFile(pic));
			imgs.put(pic.getId(), view);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 填充ImageView
		view.setScaleType(ImageView.ScaleType.FIT_XY);
		/* 设置布局参数 */
		view.setLayoutParams(new Gallery.LayoutParams(MainUI.displayMetrics.widthPixels, MainUI.displayMetrics.heightPixels));
		int mGalleryItemBackground=0;
		/* 设置背景资源 */
		view.setBackgroundResource(mGalleryItemBackground);
		return view;
	}

	private Bitmap getImageFromAssetFile(Pic pic) {
		String file = content.server + "imgs/" + pic.getId();
		Bitmap image = caches.get(pic.getId());
		if (null == image) {
			try {
				URL url = new URL(pic.getUrl());
				File catalog = new File("/sdcard/" + pic.getCatalogs());
				if (!(catalog.exists()) && !(catalog.isDirectory())) {
					boolean creadok = catalog.mkdirs();
					if (creadok) {
						System.out.println(" ok:创建文件夹成功！ ");
					} else {
						System.out.println(" err:创建文件夹失败！ ");
					}
				}
				File fileitem = new File("/sdcard/" + pic.getCatalogs() + "/"
						+ pic.getName());
				if (fileitem.exists() && fileitem.length() > 100) {
					
				} else {
					URLConnection conn = url.openConnection();
					conn.setDoInput(true);
					int length = (int) conn.getContentLength();
					InputStream is = conn.getInputStream();
					if (length != -1) {
						FileOutputStream	output = new FileOutputStream(fileitem);
						byte buffer[] = new byte[6 * 1024];
						int temp1;
						while ((temp1 = is.read(buffer)) != -1) {
							output.write(buffer,0,temp1);
						}
						output.flush();
					}
				}
				FileInputStream inputStream = new FileInputStream(fileitem);
				image = BitmapFactory.decodeStream(inputStream);
				caches.put(pic.getId(), image);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return image;
	}

}
