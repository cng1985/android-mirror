package com.baoyi.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.baoyi.domain.Pic;
import com.baoyi.factory.PicFacotry;
import com.baoyi.ui.MainUI;
import com.baoyi.widget.RemoteImageView;

public class RemoteImagesAdapter extends BaoyiBaseAdapter {
	private List<Pic> myImageURL = new ArrayList<Pic>();
	private Context myContext;

	/* 构造函数 Context */
	public RemoteImagesAdapter(String file, Context c) {
		this.myContext = c;
		myImageURL = PicFacotry.createByName("keai");
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return myImageURL.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RemoteImageView imageView = new RemoteImageView(this.myContext);
		imageView.setImageUrl(myImageURL.get(position).getUrl());
		// 填充ImageView
		imageView.setScaleType(ImageView.ScaleType.FIT_XY);
		/* 设置布局参数 */
		imageView.setLayoutParams(new Gallery.LayoutParams(
				MainUI.displayMetrics.widthPixels,
				MainUI.displayMetrics.heightPixels));
		/* 设置背景资源 */
		// imageView.setBackgroundResource(mGalleryItemBackground);
		return imageView;
	}

	@Override
	public String getPicId(int position) {
		// TODO 获取图片id
		return myImageURL.get(position).getId();
	}
	@Override
	public String getCataLogs(int position) {
		// TODO 获取图片目录
		return myImageURL.get(position).getCatalogs();
	}

}
