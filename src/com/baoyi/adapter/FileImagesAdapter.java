package com.baoyi.adapter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class FileImagesAdapter extends BaseAdapter {

	private Context mcontext;
	private int count;
	private List<File> allfiles;

	public FileImagesAdapter(String files,Context mcontext) {
		allfiles = new ArrayList<File>();
		this.mcontext=mcontext;
		try {
		String[] fs=	mcontext.getAssets().list(files);
			for (int i = 0; i < fs.length; i++) {
				String item = fs[i];
				if (item.endsWith("jpg")) {
					allfiles.add(new File(item));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public Context getMcontext() {
		return mcontext;
	}

	public void setMcontext(Context mcontext) {
		this.mcontext = mcontext;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return count;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView view = null;
		if (convertView == null) {
			view = new ImageView(mcontext);

		} else {
			view = (ImageView) convertView;
			view.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					System.out.println(v.getClass().getName());
				}
			});
		}
		Uri uri = Uri.parse(allfiles.get(position).getPath());
		view.setImageURI(uri);
		// view.setImageResource(allfiles.get(position).toURI());
		return view;
	}

}
