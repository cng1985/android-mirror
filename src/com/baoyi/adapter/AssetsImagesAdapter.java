package com.baoyi.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.baoyi.ui.MainUI;
import com.baoyi.utils.ImageUtil;

public class AssetsImagesAdapter extends BaoyiBaseAdapter {
	private Context mcontext;
	private List<String> files = new ArrayList<String>();
	private Map<String, Bitmap> caches = new HashMap<String, Bitmap>();
	private Map<String, ImageView> imgs = new HashMap<String, ImageView>();
	private String filename;

	public AssetsImagesAdapter(String file, Context mcontext) {
		filename = file;
		this.mcontext = mcontext;
		try {
			String[] fs = mcontext.getAssets().list(file);
			for (int i = 0; i < fs.length; i++) {
				String item = fs[i];
				if (item.endsWith("jpg")) {
					files.add(item);
					if (i < 3) {
						String file1 = filename + "/" + item;
						getImageFromAssetFile(file1);
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return files.size();
	}

	public ImageView getImageViewById(int id) {
		String file = filename + "/" + files.get(id);
		return imgs.get(file);
	}

	@Override
	public Object getItem(int position) {
		Bitmap image = null;
		String file = filename + "/" + files.get(position);
		image = getImageFromAssetFile(file);
		return image;
	}

	public String getPicId(int position) {
		String file = filename + "/" + files.get(position);
		return file;
	}

	public String getCataLogs(int position) {
		return filename;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView view = null;
		if (convertView == null) {
			view = new ImageView(mcontext);

		} else {
			view = (ImageView) convertView;
			view.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					System.out.println(v.getClass().getName());
				}
			});
		}
		// Uri uri = Uri.parse(allfiles.get(position).getPath());
		// view.setImageURI(uri);
		// view.setImageResource(allfiles.get(position).toURI());
		// view.setScaleType(ScaleType.FIT_XY);
		String file = filename + "/" + files.get(position);
		view.setImageBitmap(getImageFromAssetFile(file));
		// 填充ImageView
	//	view.setScaleType(ImageView.ScaleType.FIT_XY);
		/* 设置布局参数 */
		view.setLayoutParams(new Gallery.LayoutParams(
				MainUI.displayMetrics.widthPixels,
				MainUI.displayMetrics.heightPixels));
		imgs.put(file, view);
		return view;
	}

	private Bitmap getImageFromAssetFile2(String fileName) {

		// Display display = getWindowManager().getDefaultDisplay();

		Bitmap image = null;
		if (null == image) {

			try {
				AssetManager am = mcontext.getAssets();
				InputStream is = am.open(fileName);
				image = BitmapFactory.decodeStream(is);
				is.close();
			} catch (Exception e) {
			}
		}
		return image;
	}

	private Bitmap getImageFromAssetFile1(String fileName) {

		// Display display = getWindowManager().getDefaultDisplay();
		DisplayMetrics dm = new DisplayMetrics();
		dm = mcontext.getApplicationContext().getResources()
				.getDisplayMetrics();
		int screenWidth = dm.widthPixels;
		int screenHeight = dm.heightPixels;
		Bitmap image = null;
		if (null == image) {

			try {
				AssetManager am = mcontext.getAssets();
				InputStream is = am.open(fileName);
				image = BitmapFactory.decodeStream(is);
				is.close();
				image = Bitmap.createScaledBitmap(image, screenWidth,
						screenHeight, false);
				// caches.put(fileName, image);

			} catch (Exception e) {
			}
		}
		return image;
	}

	private Bitmap getImageFromAssetFile(String fileName) {

		Bitmap image = caches.get(fileName);
		if (null == image) {
			try {
				AssetManager am = mcontext.getAssets();
				InputStream is = am.open(fileName);
				image = BitmapFactory.decodeStream(is);
				image = ImageUtil.getRoundedCornerBitmap(image, 50.0f);
				image = ImageUtil.zoomBitmap(image, 320,480);
				is.close();
				caches.put(fileName, image);
			} catch (Exception e) {
			}
		}
		return image;
	}
}
