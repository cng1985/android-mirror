package com.baoyi.adapter;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import com.baoyi.content.content;
import com.baoyi.domain.Pic;
import com.baoyi.factory.PicFacotry;
import com.baoyi.ui.MainUI;

public class myInternetGalleryAdapter extends BaseAdapter {

	private Context myContext;
	private int mGalleryItemBackground;
	private List<String> myImageURL = new ArrayList<String>();
	private String catalog;
	/* 构造函数 Context */
	public myInternetGalleryAdapter(String file, Context c) {
		this.myContext = c;
		 List<Pic> ps=PicFacotry.createByName("keai");
		 catalog=file;
		 for (Pic pic : ps) {
			 String files = content.server + "imgs/" + pic.getId();
			 myImageURL.add(files);
		}
		// 检索 这方面的主题风格的属性
		// TypedArray a = myContext.obtainStyledAttributes(R.styleable.Gallery);

		// 得到资源标识
		// mGalleryItemBackground = a.getResourceId(
		// R.styleable.Gallery_android_galleryItemBackground, 0);
		//
		// // 返回 TypedArray
		// a.recycle();

	}

	/* */
	public int getCount() {
		return myImageURL.size();
	}

	/* ID */
	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	/* */
	public float getScale(boolean focused, int offset) {
		/* Formula: 1 / (2 ^ offset) */
		return Math.max(0, 1.0f / (float) Math.pow(2, Math.abs(offset)));
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		/* ImageView */

		ImageView imageView = new ImageView(this.myContext);
		try {
			URL aryURI = new URL(myImageURL.get(position));
			/* 打开连接 */
			URLConnection conn = aryURI.openConnection();
			conn.connect();
			/* 转变为 InputStream */
			InputStream is = conn.getInputStream();
			/* 将InputStream转变为Bitmap */
			Bitmap bm = BitmapFactory.decodeStream(is);
			/* 关闭InputStream */
			is.close();
			/* 添加图片 */
			imageView.setImageBitmap(bm);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// 填充ImageView
		imageView.setScaleType(ImageView.ScaleType.FIT_XY);
		/* 设置布局参数 */
		imageView.setLayoutParams(new Gallery.LayoutParams(MainUI.displayMetrics.widthPixels, MainUI.displayMetrics.heightPixels));
		/* 设置背景资源 */
		imageView.setBackgroundResource(mGalleryItemBackground);
		return imageView;
	}

	public String getPicId(int selectIndex) {
		String id=catalog+"/"+myImageURL.get(selectIndex);
		return id;
	}

	public String getCataLogs(int selectIndex) {
		// TODO Auto-generated method stub
		return catalog;
	}
}
