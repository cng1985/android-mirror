package com.baoyi.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.baoyi.control.AndroidBigImageView;
import com.baoyi.ui.R;

public class ImageAdapter extends BaseAdapter {

	private Context mcontext;

	public Context getMcontext() {
		return mcontext;
	}

	public void setMcontext(Context mcontext) {
		this.mcontext = mcontext;
	}

	public ImageAdapter() {
		imgs = new ArrayList<Integer>();
	}

	public int getCount() {

		return imgs.size();
	}

	public Object getItem(int arg0) {

		return null;
	}

	public int getImageId(int id) {
		return imgs.get(id);
	}

	public long getItemId(int position) {

		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		AndroidBigImageView view = null;
		if (convertView == null) {
			view = new AndroidBigImageView(mcontext);

		} else {
			view = (AndroidBigImageView) convertView;
			view.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					System.out.println(v.getClass().getName());
				}
			});
		}
		view.setImageResource(imgs.get(position));
		return view;
	}

	private List<Integer> imgs;
}
