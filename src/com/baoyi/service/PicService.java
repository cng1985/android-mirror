package com.baoyi.service;

import java.io.IOException;

import com.baoyi.domain.Pic;
import com.baoyi.utils.Connection;
import com.baoyi.utils.Connection.Method;
import com.baoyi.utils.HttpConnection;

public class PicService {

	public Pic works(Pic pic) {
		Connection con = HttpConnection
				.connect("http://www.xabaoyi.com:8080/picweb/VoteServlet/");

		con.data("id", pic.getId());
		con.data("catalogs", pic.getCatalogs());
		con.timeout(2000);
		con.request().method(Method.POST);
		try {
			String repose = con.execute().body();
			repose = repose.trim();
			pic.setScore(Long.parseLong(repose));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return pic;

	}
}
