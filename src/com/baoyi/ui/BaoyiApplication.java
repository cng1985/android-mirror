package com.baoyi.ui;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.baoyi.utils.ImageCache;

public class BaoyiApplication extends Application {
	/**
	 * Singleton pattern
	 */
	private static BaoyiApplication instance;
	/**
	 * Tag used for DDMS logging
	 */
	public static String TAG = "baoyi";

	public static BaoyiApplication getInstance() {
		return instance;
	}

	/**
	 * Image cache, one for all activities and orientations
	 */
	private ImageCache mImageCache;

	/**
	 * Access to global image cache across Activity instances
	 * 
	 * @return
	 */
	public ImageCache getImageCache() {
		return mImageCache;
	}

	/**
	 * Retrieves application's version number from the manifest
	 * 
	 * @return
	 */
	public String getVersion() {
		String version = "0.0.0";

		PackageManager packageManager = getPackageManager();
		try {
			PackageInfo packageInfo = packageManager.getPackageInfo(
					getPackageName(), 0);
			version = packageInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		return version;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		mImageCache = new ImageCache();
	}

	public boolean isonline() {
		ConnectivityManager cManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = cManager.getActiveNetworkInfo();
		if (info != null && info.isAvailable()) {
			// do something
			// 能联网
			return true;
		} else {
			// 不能联网
			return false;
		}
	}
}
