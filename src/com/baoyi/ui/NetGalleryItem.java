package com.baoyi.ui;

import android.widget.Toast;

import com.baoyi.adapter.RemoteImagesAdapter;

public class NetGalleryItem  extends BaoyiGallery  {


	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void setwallpaperManager() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void initAdapter() {
		
		adapter = new RemoteImagesAdapter(fileitem, this);
	}

	@Override
	protected void oninitApp() {
		if (BaoyiApplication.getInstance().isonline()) {
			// do something
			// 能联网
			fileitem = (String) getIntent().getExtras().get("fileitem");
		} else {
			// do something
			// 不能联网
			Toast.makeText(NetGalleryItem.this, "请检查你的网络！", Toast.LENGTH_SHORT)
					.show();
			return;
		}
		
	}

}
