package com.baoyi.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.GestureDetector;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.baoyi.utils.Utils;

public class MainUI extends Activity {
	LayoutParams layoutParams;
	GestureDetector mGestureDetecto;
	public static DisplayMetrics displayMetrics;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		/** 全屏设置，隐藏窗口所有装饰 */
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		/** 标题是属于View的，所以窗口所有的修饰部分被隐藏后标题依然有效 */
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		layoutParams = new LinearLayout.LayoutParams(43, 35);

		initUI();

		Display display = getWindowManager().getDefaultDisplay();
		System.out.println("getWidth:" + display.getWidth() + " getHeight:"
				+ display.getHeight());
		// if (Utils.isWifi(this)) {
		// Toast.makeText(MainUI.this, "你用的wifi上网", Toast.LENGTH_SHORT).show();
		// }
		displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		
		
	}

	/**
	 * 可爱美女按钮
	 */
	private ImageView kabutton;
	/**
	 * 性感美女按钮
	 */
	private ImageView xgbutton;
	/**
	 * 知性美女按钮
	 */
	private ImageView zxbutton;
	/**
	 * 清纯美女按钮
	 */
	private ImageView qcbutton;
	/**
	 * 制服美女按钮
	 */
	private ImageView zfbutton;
	/**
	 * 明星美女按钮
	 */
	private ImageView mxbutton;

	public void initUI() {
		kabutton = (ImageView) findViewById(R.id.imageviewka);
		kabutton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent item = new Intent(MainUI.this, NetGalleryItem.class);
				item.putExtra("fileitem", "keai");
				startActivityForResult(item, RESULT_OK);
			}
		});
		xgbutton = (ImageView) findViewById(R.id.imageviewxg);
		xgbutton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent item = new Intent(MainUI.this, GalleryItem.class);
				item.putExtra("fileitem", "xinggan");
				startActivityForResult(item, RESULT_OK);
			}
		});
		zxbutton = (ImageView) findViewById(R.id.imageviewzx);
		zxbutton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent item = new Intent(MainUI.this, GalleryItem.class);
				item.putExtra("fileitem", "zhixing");
				startActivityForResult(item, RESULT_OK);
			}
		});
		qcbutton = (ImageView) findViewById(R.id.imageviewqc);
		qcbutton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent item = new Intent(MainUI.this, GalleryItem.class);
				item.putExtra("fileitem", "qingchun");
				startActivityForResult(item, RESULT_OK);
			}
		});
		zfbutton = (ImageView) findViewById(R.id.imageviewzf);
		zfbutton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent item = new Intent(MainUI.this, GalleryItem.class);
				item.putExtra("fileitem", "zihu");
				startActivityForResult(item, RESULT_OK);
			}
		});
		mxbutton = (ImageView) findViewById(R.id.imageviewmx);
		mxbutton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				Intent item = new Intent(MainUI.this, GalleryItem.class);
				item.putExtra("fileitem", "star");
				startActivityForResult(item, RESULT_OK);
			}
		});
	}
	public String getLocalMacAddress() {
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		WifiInfo info = wifi.getConnectionInfo();
		return info.getMacAddress();
	}
}