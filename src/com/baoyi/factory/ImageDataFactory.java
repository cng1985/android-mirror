package com.baoyi.factory;

import android.content.Context;

import com.baoyi.adapter.AssetsImagesAdapter;

public class ImageDataFactory {

	public AssetsImagesAdapter create(String filename, Context mcontext) {
		AssetsImagesAdapter adapter = new AssetsImagesAdapter(filename,
				mcontext);
		return adapter;
	}
}
