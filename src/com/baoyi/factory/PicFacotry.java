package com.baoyi.factory;

import java.io.StringReader;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.baoyi.content.content;
import com.baoyi.domain.Pic;
import com.baoyi.utils.Connection;
import com.baoyi.utils.HttpConnection;
import com.baoyi.xml.hander.ImageHandler;

public class PicFacotry {

	public static List<Pic> createByName(String name){
		 List<Pic> filesa = null;
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			String urls = content.picserver + "data/main_directory.xml";
			URL url = new URL(urls);
			Connection urlConn = HttpConnection.connect(url);
			SAXParser parser = factory.newSAXParser();
			XMLReader xmlreader = parser.getXMLReader();
			ImageHandler healHandler = new ImageHandler(name);
			xmlreader.setContentHandler(healHandler);
			String xmldata = urlConn.execute().body();
			xmlreader.parse(new InputSource(new StringReader(xmldata)));
			filesa = healHandler.getPics();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filesa;
		
	}
}
